import React, { Component } from "react";
import "./App.scss";
import Button from "./Components/Button";
import Modal from "./Components/Modal";
import ButtonsSet from './Components/ButtonsSet'

window.addEventListener("click", (e) => {
  if (e.target.hasAttribute("data-modal-window")) {
    e.target.style.display = "none";
  }
});

function openModal(ev) {
  let name = ev.target.getAttribute("data-modal-btn");
  let modal = document.querySelector(`[data-modal-window=${name}]`);
  modal.style.display = "block";
  let close = modal.querySelector(".close_modal_window");
  close.addEventListener("click", () => {
    modal.style.display = "none";
  });
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="buttons">
          <Button
            backgroundColor="btn-primary"
            text="Open first modal"
            data="firstModal"
            onClick={openModal}
          />
          <Button
            backgroundColor="btn-secondary"
            text="Open second modal"
            data="secondModal"
            onClick={openModal}
          />
        </div>
        <div className="modals">
          <Modal
            header="Do u want blue modal?"
            closeButton={true}
            text="The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested."
            data="firstModal"
            backgroundColor='bgc-red'
            actions={
              <ButtonsSet
              className='modal_buttons'
              buttonsColor="modal_btn"
              />
            }
          />
          <Modal
            header="Do u want red modal?"
            closeButton={false}
            text="Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            data="secondModal"
            backgroundColor='bgc-blue'
            actions={
              <ButtonsSet
              className='modal_buttons'
                buttonsColor="modal_btn"
              />}
          />
        </div>
      </div>
    );
  }
}

export default App;