import React from "react";
import Button from "../Button";

class ButtonsSet extends React.Component {
  render = () => (
    <div className={this.props.className}>
      <Button
        backgroundColor={this.props.buttonsColor}
        text="OK"
        data={false}
        onClick={false}
      />
      <Button
        backgroundColor={this.props.buttonsColor}
        text="Cancel"
        data={false}
        onClick={false}
      />
    </div>
  );
}
export default ButtonsSet;
