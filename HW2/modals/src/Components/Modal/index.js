import React from "react";

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.closeButton = this.props.closeButton
    this.action = this.props.action
  }
  createCloseButton=()=>{
    console.log(this.closeButton);
    if (this.closeButton) {
        return <div className="close_modal_window">&times;</div>
    }
  }
  render = () => (
    <div data-modal-window={this.props.data} className="modal">
      <div className={"modal_content "+this.props.backgroundColor}>
        <div className="modal_header">
          <div className="modal_title">{this.props.header}</div>
          {this.createCloseButton()}
        </div>
        <div className="modal_body">
          <span className="modal_text">{this.props.text}</span>
          {this.props.actions}
        </div>
      </div>
    </div>
  );
}
export default Modal;
