import React from "react";

class Button extends React.Component{
    constructor(props){
        super(props)
        this.className = `btn ${this.props.backgroundColor}`
        this.data = this.props.data
        this.onClick = this.props.onClick
    }
    validData=()=>{
        if (this.data) {
            return this.data          
        }else{
            return 
        }
    }    
    validClick=()=>{
        if (this.onClick) {
            return this.onClick          
        }else{
            return 
        }
    }    
    render = ()=>(
        <button className={this.className}  data-modal-btn={this.validData()} onClick={this.validClick()}>{this.props.text}</button>
    )
}
export default Button

