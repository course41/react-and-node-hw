let btns = document.querySelectorAll("*[data-modal-btn]");
btns.forEach((btn, index) => {
  btn.addEventListener("click", () => {
    let name = btn.getAttribute("data-modal-btn");
    let modal = document.querySelector(`[data-modal-window=${name}]`);
    modal.style.display = "block";
    let close = modal.querySelector(".close_modal_window");
    close.addEventListener("click", () => {
      modal.style.display = "none";
    });
  });
});

window.addEventListener("click", (e) => {
  if (e.target.hasAttribute("data-modal-window")) {
    e.target.style.display = "none";
  }
});

// window.onclick = function (e){
//     if (e.target.hasAttribute('data-modal-window')) {
//         e.target.style.display='none'
//         // let modals = document.querySelectorAll('*[data-modal-window]')

//         // modals.forEach((modal)=>{
//         //     modal.style.display='none'
//         // })
//     }
// }
